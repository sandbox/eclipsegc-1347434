api = 2
core = 7.x

projects[ctools][type] = module
projects[ctools][download][type] = git
projects[ctools][download][url] = http://git.drupal.org/project/ctools.git
projects[ctools][download][branch] = 7.x-1.x
projects[ctools][patch][] = "http://drupal.org/files/page-manager-admin-paths_3.patch"
projects[ctools][patch][] = "http://drupal.org/files/entity-child-plugin.patch"
projects[ctools][patch][] = "http://drupal.org/files/empty-context.patch"

projects[references][type] = module
projects[references][version] = 2.x-dev

projects[context_admin][type] = module
projects[context_admin][version] = 1.1

projects[panels][type] = module
projects[panels][version] = 3.x-dev

projects[features][type] = module
projects[features][version] = 1.x-dev

projects[strongarm][type] = module
projects[strongarm][version] = 2.0-beta4

projects[views][type] = module
projects[views][download][type] = git
projects[views][download][url] = http://git.drupal.org/project/views.git
projects[views][download][branch] = 7.x-3.x

projects[views_bulk_operations][type] = module
projects[views_bulk_operations][version] = 3.0-beta3

projects[entity][type] = module
projects[entity][version] = 1.x-dev

projects[ca_news][type] = module
projects[ca_news][download][type] = git
projects[ca_news][download][url] = eclipsegc@git.drupal.org:sandbox/eclipsegc/1345384.git
projects[ca_news][download][branch] = master
